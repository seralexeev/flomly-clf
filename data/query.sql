SELECT DISTINCT
    ON (q.id) q.id, q.name, group_id, g.name, count(group_id) AS count
FROM 
    (SELECT pn.id,
         pn.name,
         pc.group_id
    FROM products pn
    JOIN LATERAL 
        (SELECT group_id
        FROM products
        WHERE group_id IS NOT NULL
        ORDER BY  name <-> pn.name LIMIT 3) pc
            ON 1 = 1
        WHERE pn.group_id IS NULL LIMIT 50) q
    JOIN product_groups g
    ON g.id = q.group_id
GROUP BY  q.id, q.name, g.name, group_id
ORDER BY  q.id, count DESC;