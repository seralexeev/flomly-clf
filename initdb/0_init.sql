create extension pg_trgm;

create table product_groups (
	id serial primary key,
	name text not null
);

create unique index product_groups_name_unique_idx on product_groups (lower(name));

create table products (
	id serial primary key,
	name text not null,
	group_id int references product_groups (id)
);