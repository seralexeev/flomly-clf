\copy product_groups(name) FROM '/var/groups.csv' DELIMITER ',' CSV;
\copy products(name, group_id) FROM '/var/products.csv' DELIMITER ',' CSV;
\copy products(name) FROM '/var/products_unclassified.csv' DELIMITER ',' CSV;