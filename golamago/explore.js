const products = require('./all_products.json');

const dataset = products.map(({ name, group_name }) => ({
    name, group_name
}));

console.log(JSON.stringify(dataset));