const axios = require("axios").default;
const fs = require("fs");
const { promisify } = require("util");
const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);

// GET /products/ae272975-9ffb-47c9-a5aa-3b9e85397545?categoryId=1La2Ml&priceMax=99999&priceMin=0&skip=80&take=20&useFavourites=false HTTP/1.1
// Host: api.golamago.online
// Content-Type: Application/json
// X-ClientSettings: { "search" : "sort" }
// UniqueId: F45962EC-5212-4A47-BCED-8FE25334E571
// Connection: keep-alive
// Accept: */*
// User-Agent: iPhone10,5;ios-12.4.1;client-1.6.8
// Accept-Language: en-us
// Accept-Encoding: br, gzip, deflate

const get = url => {
    return axios.get("https://api.golamago.online" + url, {
        headers: {
            "User-Agent": "iPhone10,5;ios-12.4.1;client-1.6.8",
            "X-ClientSettings": '{ "search" : "sort" }',
            UniqueId: "F45962EC-5212-4A47-BCED-8FE25334E571",
            Authorization: "... "
        }
    });
};

(async () => {
    try {
        const groups = await getgroups();
        let res = [];

        for (const [groupId, groupName] of Object.entries(groups)) {
            const {
                data: { items }
            } = await get(
                `/products/ae272975-9ffb-47c9-a5aa-3b9e85397545?categoryId=${groupId}&priceMax=99999&priceMin=0&skip=0&take=10000&useFavourites=false`
            );

            for (const item of items) {
                item.group_id = groupId;
                item.group_name = groupName;
            }

            res = res.concat(items.map(({name, description} )=> ({
                name, description, group_id: groupId, group_name: groupName
            })));

            console.log(
                `completed ${groupName} (${groupId}) count: ${items.length}`
            );
        }

        await writeFile(`./all_products2.json`, JSON.stringify(res));
    } catch (e) {
        console.log(e);
    }
})();

async function getgroups() {
    const { data } = await get(
        "/categories/ae272975-9ffb-47c9-a5aa-3b9e85397545"
    );
    return traverseGroups(data);
}

function traverseGroups(groups, result = {}) {
    for (const group of groups) {
        if (!group.isHiddenFromPacker) {
            result[group.id] = group.name;
        }

        traverseGroups(group.sub, result);
    }

    return result;
}
