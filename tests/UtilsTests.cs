using NUnit.Framework;
using flomly;

namespace tests {
    public class UtilsTests {
        [Test]
        public void GenerateNGrams() {
            var res = Utils.GenerateNGrams("hello", 3);
            Assert.AreEqual(3, res.Length);
            Assert.AreEqual(res[0], "hel");
            Assert.AreEqual(res[1], "ell");
            Assert.AreEqual(res[2], "llo");
        }
    }
}