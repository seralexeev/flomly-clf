using System.Collections;
using System.Collections.Generic;

namespace flomly {
    public class SparseMatrix<T> : IEnumerable<Dictionary<int, T>> {
        Dictionary<long, T> _cells = new Dictionary<long, T>();

        public int Columns { get; private set; }
        public int Rows { get; private set; }

        public SparseMatrix(int columns, int rows) => (Columns, Rows) = (columns, rows);

        public T this[int row, int col] {
            get => _cells.TryGetValue(row * Columns + col, out var result) ? result : default;
            set => _cells[row * Columns + col] = value;
        }

        public Dictionary<int, T> this[int row] {
            get {
                var res = new Dictionary<int, T>();
                for (var i = 0; i < Columns; i++) {
                    if (_cells.TryGetValue(row * Columns + i, out var item)) {
                        res[i] = item;
                    }
                }

                return res;
            }
        }

        public IEnumerator<Dictionary<int, T>> GetEnumerator() {
            for (var i = 0; i < Rows; i++) {
                yield return this[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}