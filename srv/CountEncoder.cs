using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using static flomly.Utils;

namespace flomly {
    public class CountEncoder : IVectorEncoder {
        Regex _regexp = new Regex("[^a-zа-я ]", RegexOptions.Compiled | RegexOptions.CultureInvariant);
        Dictionary<string, int> _vocabulary;

        public IVectorEncoder Fit(string[] ds) {
            _vocabulary = ds.SelectMany(x => Preprocess(x))
                .Distinct()
                .Select((token, index) => (token, index))
                .ToDictionary(x => x.token, x => x.index);

            return this;
        }

        public SparseMatrix<int> Transform(string[] ds) {
            if (_vocabulary == null) {
                throw new InvalidOperationException("Encoder hasn't been trained");
            }

            // можно сделать в 1 проход и эффективно (сейчас супер неэфективно)
            var matrix = new SparseMatrix<int>(_vocabulary.Count, ds.Length);
            for (var i = 0; i < ds.Length; i++) {
                var item = ds[i];
                foreach (var grm in Preprocess(item)) {
                    if (_vocabulary.TryGetValue(grm, out var col)) {
                        matrix[i, col] += 1;
                    }
                }
            }

            return matrix;
        }

        IEnumerable<string> Preprocess(string item) {
            var str = _regexp.Replace(item.Trim().ToLowerInvariant(), " ");
            var tokens = str.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            foreach (var token in tokens) {
                var ngrams = GenerateNGrams(token, 3);

                foreach (var grm in ngrams) {
                    yield return grm;
                }
            }
        }
    }

    public interface IVectorEncoder {
        IVectorEncoder Fit(string[] ds);
        SparseMatrix<int> Transform(string[] ds);
    }
}