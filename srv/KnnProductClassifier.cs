using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace flomly {
    public class KnnProductClassifier : IProductClassifier {
        readonly int _k;

        readonly IVectorEncoder _encoder;
        readonly SparseMatrix<int> _instances;
        readonly string[] _labels;

        public int InstanceCount => _labels.Length;

        // clone encoder before
        public KnnProductClassifier(Product[] instances, IVectorEncoder encoder, int k = 5) {
            var instancesVecotor = instances.Select(x => x.Name).ToArray();
            _encoder = encoder.Fit(instancesVecotor);
            _instances = _encoder.Transform(instancesVecotor);
            _labels = instances.Select(x => x.GroupName).ToArray();
            _k = k;
        }

        public (string, double) ClassifyByGroup(string name) {
            if (string.IsNullOrEmpty(name)) {
                throw new ArgumentException("message", nameof(name));
            }

            var vec = _encoder.Transform(new[] { name })[0];
            var label = _instances
                .Select((x, index) => (distance: Utils.CosineSimilarity(vec, x), index))
                .OrderByDescending(x => x.distance)
                .Take(_k)
                .Select(x => (index: _labels[x.index], x.distance))
                .GroupBy(x => x.index)
                .OrderByDescending(x => x.Count())
                .Select(x => (x.Key, x.OrderByDescending(z => z.distance).FirstOrDefault().distance))
                .FirstOrDefault();

            return label;
        }
    }

    public class JsonFileKnnProductClassifier : IProductClassifier {
        readonly string _fileName;
        readonly IVectorEncoder _encoder;
        readonly int _k;
        KnnProductClassifier _inner;

        public int InstanceCount => _inner.InstanceCount;

        public JsonFileKnnProductClassifier(string fileName, IVectorEncoder encoder, int k = 5) {
            _fileName = fileName;
            _encoder = encoder;
            _k = k;
        }

        public JsonFileKnnProductClassifier Initialize() {
            var serializer = new JsonSerializer();
            using (var file = File.OpenText(_fileName))
            using (var reader = new JsonTextReader(file)) {
                var products = serializer.Deserialize<Product[]>(reader);
                _inner = new KnnProductClassifier(products, _encoder, _k);
            }

            return this;
        }

        public (string, double) ClassifyByGroup(string name) => _inner.ClassifyByGroup(name);
    }
}