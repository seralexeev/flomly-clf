using Newtonsoft.Json;

namespace flomly {
    public class Product {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("group_name")]
        public string GroupName { get; set; }
    }
}