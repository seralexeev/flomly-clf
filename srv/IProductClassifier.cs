namespace flomly {
    public interface IProductClassifier {
        (string, double) ClassifyByGroup(string name);
    }
}