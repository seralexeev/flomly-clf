﻿using System;
using System.Diagnostics;

namespace flomly {
    class Program {
        static void Main(string[] args) {
            var k = 3;
            var initsw = Stopwatch.StartNew();
            Console.WriteLine($"Initializing instance-based knn model with k={k}...");
            var clf = new JsonFileKnnProductClassifier(@"../data/data-ready.json", new CountEncoder(), k)
                .Initialize();

            Console.WriteLine($"Completed with {clf.InstanceCount} instances [{initsw.ElapsedMilliseconds}ms]");
            var text = "";
            while (true) {
                Console.WriteLine("Please enter product name\n");

                while ((text = Console.ReadLine()) != "") {
                    var sw = Stopwatch.StartNew();
                    var (group, sim) = clf.ClassifyByGroup(text);
                    Console.WriteLine($"{text} -> {group} ({sim.ToString("N2")}) [{sw.ElapsedMilliseconds}ms]");
                }
            }
        }
    }
}
