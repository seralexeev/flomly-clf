using System;
using System.Collections.Generic;
using System.Linq;

namespace flomly {
    public static class Utils {
        public static string[] GenerateNGrams(string str, int n) {
            return Impl().ToArray();
            IEnumerable<string> Impl() {
                for (var i = 0; i <= str.Length - n; i++) {
                    yield return str.Substring(i, n);
                }
            }
        }

        public static double CosineSimilarity<T>(Dictionary<T, int> s1, Dictionary<T, int> s2) {
            return DotProduct() / (Norm(s1) * Norm(s2));

            double DotProduct() {
                var small = s2;
                var large = s1;

                if (s1.Count < s2.Count) {
                    small = s1;
                    large = s2;
                }

                return small.Aggregate(
                    0d, (acc, item) => large.TryGetValue(item.Key, out var i) ? acc + 1.0 * item.Value * i : acc);
            }

            double Norm(Dictionary<T, int> s) =>
                 Math.Sqrt(s.Aggregate(0d, (acc, item) => acc += 1.0 * item.Value * item.Value));
        }
    }
}